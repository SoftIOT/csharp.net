﻿﻿using ConsoleLinqSql.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleLinqSql
{
    class Program
    {
        static void Main(string[] args)
        {
            using (NorthwindEntities database = new NorthwindEntities())
            {
                Console.WriteLine("Hello Northwind Database\n");

                IQueryable<Product> source = database.Products;
                IQueryable<Product> results = source.Where(product => product.UnitPrice > 100);

                foreach (Product item in results) {
                    Console.WriteLine("{0}: {1}", item.ProductName, item.UnitPrice);
                }

                
                Console.WriteLine(" --------------- ");
                results = source.Where(product => product.UnitPrice < 20 || product.UnitPrice > 90);
                foreach (Product item in results)
                {
                    Console.WriteLine("{0}: {1}", item.ProductName, item.UnitPrice);
                }

                Console.WriteLine(" --------------- ");
                results = source.Where(product => product.UnitPrice < 20)
                                .Where(prod => prod.UnitPrice > 10);
                foreach (Product item in results)
                {
                    // Console.WriteLine("{0}: {1}", item.ProductName, item.UnitPrice);
                    Console.WriteLine("{0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}", item.ProductID, item.ProductName, item.SupplierID, item.CategoryID, item.QuantityPerUnit, item.UnitPrice, item.UnitsInStock, item.UnitsOnOrder, item.ReorderLevel, item.Discontinued);
                }

                Console.WriteLine(" --------------- ");
                results = source.Where(product => product.ProductName.StartsWith("C"));
                foreach (Product item in results) { Console.WriteLine("{0}: {1}", item.ProductName, item.UnitPrice); }

                Console.WriteLine(" --------------- ");
                // IEnumerable<string> names = new string[] { "Chai", "Chang", "Tofu" };
                string[] names = new string[] { "Chai", "Chang", "Tofu" };
                results = source.Where(product => names.Contains(product.ProductName));
                foreach (Product item in results) { Console.WriteLine("{0}: {1}, {2}", item.ProductID, item.ProductName, item.UnitPrice); }

                Console.WriteLine(" --------------- ");
                // results = source.Where(product => product.CategoryID != 2);
                IEnumerable<Product> res = source.Where(product => product.CategoryID != null);
                foreach (Product item in res) { Console.WriteLine("{0}: {1}, {2}", item.ProductID, item.ProductName, item.UnitPrice); }

                Console.WriteLine(" ! --------------- ");
                var result = source.Where(product => product.UnitPrice > 100)
                                   .Select(product => new
                                   {
                                       product.ProductName,
                                       product.UnitPrice,
                                       product.CategoryID
                                   })
                                    .AsEnumerable()
                                    .Select(item => new Product()
                                    {
                                        ProductName = item.ProductName,
                                        UnitPrice = item.UnitPrice
                                    });
                foreach (Product item in result) { Console.WriteLine("{0}: {1}, {2}", item.ProductName, item.UnitPrice, item.CategoryID); }

                Console.WriteLine(" ! --------------- ");
                result = source.Where(product => product.ReorderLevel > 20) // 1. IZ Liste source slozi listu kojoj je ReorderLevel > 20
                                .OrderBy(product => product.ProductName)     // 2. Iz (1.) prethodno slozene liste slozi listu po abecedi za ProductName
                                .OrderBy(product => product.UnitPrice)       // 3. Iz (2.) prethodno slozene liste, ako ima istih po ProuductName, tada te slozi                                   
                                .AsEnumerable() // Bez Enumerable vraca gresku. Moras slozit u enumerable listu
                                .Select(product => new Product // .Select vraca kolone koje ovdje navedes, koje želiš da budu u var result!
                                {
                                    ProductName = product.ProductName,
                                    UnitPrice = product.UnitPrice
                                });                                    // po cijeni od manje prema vecoj !!! 
                foreach (Product item in result)
                { Console.WriteLine("{0}: {1}", item.ProductName, item.UnitPrice); }

                Console.WriteLine(" ! --------------- ");
                result = source.Where(product => product.ReorderLevel > 20)
                               .OrderBy(product => product.ProductName)
                               .ThenBy(prod => prod.UnitPrice)
                               .AsEnumerable()
                               .Select(proizvod => new Product()
                               {
                                   ProductName = proizvod.ProductName,
                                   UnitPrice = proizvod.UnitPrice,
                                   ReorderLevel = proizvod.ReorderLevel
                               });

                foreach (Product item in result)
                  { Console.WriteLine("{0}: {1}, {2}", item.ProductName, item.UnitPrice, item.ReorderLevel); }

                

                Console.WriteLine(" * --------------- ");
                IQueryable<Product> outer = database.Products;    // IQueryable<Product>
                IQueryable<Category>  inner = database.Categories;
                /* result = outer.Where(product => product.UnitPrice > 100)
                              .Join(
                                 inner,
                                 product => product.CategoryId,
                                 category => category.CategoryId,
                                 (product, category) => new {

                                     ProductName = product.ProductName,
                                     UnitPrice = product.UnitPrice,
                                     CategoryName = category.CategoryName
                                  }
                                 ); */

                var result = from product in outer
                             where product.UnitPrice > 100
                             select product;
                             /* select new Product()
                                 {
                                    ProductName = product.ProductName,
                                    UnitPrice = product.UnitPrice
                                 }; */

              var  resul = outer.Where(product => product.UnitPrice > 100)
                              .OrderBy(product => product.UnitPrice)
                           // .AsQueryable()
                              .AsEnumerable()
                              .Select(product => new Product {
                                  ProductName = product.ProductName,
                                  UnitPrice = product.UnitPrice,
                                  ReorderLevel = product.ReorderLevel
                              });
             
                
                // foreach (Product item in result) { Console.WriteLine("{0}: {1}, {2}", item.ProductName, item.UnitPrice, item.ReorderLevel); }
                foreach (Product item in resul) { Console.WriteLine("{0}: {1}", item.ProductName, item.UnitPrice); }

                

            } 

            Console.WriteLine("Hello World\n");
        }
    }
}
