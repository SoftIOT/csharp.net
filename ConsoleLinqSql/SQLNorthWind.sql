﻿
select * from Products

/* pr. 1 */
select [t0].[ProductName], [t0].[UnitPrice] from [dbo].[Products] as [t0]
select [t0].[ProductName], [t0].[UnitPrice] from [dbo].[Products] as [t0] where [t0].[UnitPrice] > 100
exec sp_executesql N'SELECT [t0].[ProductID], [t0].[ProductName], [t0].[SupplierID], [t0].[CategoryID], [t0].[QuantityPerUnit], [t0].[UnitPrice], [t0].[UnitsInStock], [t0].[UnitsOnOrder], [t0].[ReorderLevel], [t0].[Discontinued]
FROM [dbo].[Products] AS [t0]
WHERE [t0].[UnitPrice] > @p0',N'@p0 decimal(33,4)',@p0=100.0000

/* pr. 2 */
select [t0].[ProductId], [t0].[ProductName], [t0].[SupplierID], [t0].[CategoryID], [t0].[QuantityPerUnit],
       [t0].UnitPrice, [t0].[UnitsInStock], [t0].[UnitsOnOrder], [t0].[ReorderLevel], [t0].[Discontinued]
 from [dbo].[Products] as [t0]
 where [t0].[UnitPrice] < 20 OR [t0].[UnitPrice] > 90  

 select [t0].[ProductId], [t0].[ProductName], [t0].[SupplierID], [t0].[CategoryID], [t0].[QuantityPerUnit],
       [t0].UnitPrice, [t0].[UnitsInStock], [t0].[UnitsOnOrder], [t0].[ReorderLevel], [t0].[Discontinued]
 from [dbo].[Products] as [t0]
 where [t0].[UnitPrice] < 20 AND [t0].[UnitPrice] > 10  

 /* pr. 3 */
 select [t0].[ProductId], [t0].[ProductName], [t0].[SupplierID], [t0].[CategoryID], [t0].[QuantityPerUnit],
       [t0].UnitPrice, [t0].[UnitsInStock], [t0].[UnitsOnOrder], [t0].[ReorderLevel], [t0].[Discontinued]
 from [dbo].[Products] as [t0]
 where [t0].[ProductName] LIKE 'Choco%'
 -- where [t0].[ProductName] LIKE '%time%'
 -- where [t0].[ProductName] LIKE '%tea'

 /* pr. 4 */
 select [t0].[ProductId], [t0].[ProductName], [t0].[SupplierID], [t0].[CategoryID], [t0].[QuantityPerUnit]
 --    ,[t0].UnitPrice, [t0].[UnitsInStock], [t0].[UnitsOnOrder], [t0].[ReorderLevel], [t0].[Discontinued]
 from [dbo].[Products] as [t0]
 where [t0].[ProductName] IN ('Chai', 'Chang', 'Tofu')

 select [tP].[ProductId], [tP].[ProductName], [tP].[SupplierID], [tP].[CategoryID], [tP].[QuantityPerUnit]
 from [dbo].[Products] as [tP]
 where [tP].[QuantityPerUnit] IN ('box', 'oz', 'gift')

 /* pr. 5 */
 select [tP].[ProductId], [tP].[ProductName], [tP].[SupplierID], [tP].[CategoryID], [tP].[QuantityPerUnit]
 from [dbo].[Products] as [tP]
 where [tP].[CategoryID] IS NOT null

 /* pr. 6 */
 select [t0].[ProductName], [t0].UnitPrice
 from [dbo].[Products] as [t0]
 where [t0].UnitPrice > 100

 /* pr. 7 */
 select [t0].[ProductName], [t0].[ReorderLevel], [t0].[UnitPrice],
 (
 CASE
  -- WHEN [t0].[UnitPrice] < 10 THEN 1
  -- WHEN NOT [t0].[UnitPrice] < 10 THEN 0
  WHEN [t0].[UnitPrice] < 10 THEN 'Skupo'
  WHEN NOT [t0].[UnitPrice] < 10 THEN 'Jeftino'
  ELSE NULL
 END
 ) AS [IsExpensive] -- ovo je nova kolona IsExpensive
 from [dbo].[Products] as [t0] 
 where [t0].[ReorderLevel] > 20

 /* pr. 8 */
 select [t0].[ProductName], [t0].[UnitPrice], [t0].[ReorderLevel]
 from [dbo].[Products] as [t0]
 where [t0].[ReorderLevel] > 20  -- and [t0].[UnitPrice] = 21
 order by [t0].[UnitPrice], [t0].[ProductName] -- desc -- Prvo slaze po stupcu UnitPrice, a onda ako ih ima iste cijene, tada nabecedno po ProductName

 select [t0].[ProductName], [t0].[UnitPrice]
 from [dbo].[Products] as [t0]
 where [t0].[ReorderLevel] > 20
 order by [t0].[ProductName], [t0].[UnitPrice] -- Prvo slozi po imenu, onda po manjoj prema vecoj cijeni

 /* pr. 9 */
 select [t1].[CategoryName]
 from [dbo].[Categories] as [t1]

 -- Odaberi sve Products po kategorijama, poredene po cijenama
 select [t0].[ProductName], [t0].[UnitPrice], [t1].[CategoryName]
 from [dbo].[Products] as [t0]
 inner join [dbo].[Categories] AS [t1] on [t0].[CategoryID] = [t1].[CategoryID]
 -- where [t1].[CategoryName] = 'seafood' -- 'lol'
 -- where [t0].[UnitPrice] > 100
 order by [t1].[CategoryName], [t0].[UnitPrice]

 -- Iz zeljene Kategorije odaberi sve Proizvode
 select [t0].[CategoryName], [t1].[ProductName], [t1].[UnitPrice]
 from [dbo].[Categories] as [t0]
 inner join [dbo].[Products] as [t1] on [t0].[CategoryID] = [t1].[CategoryID]
 -- where [t1].[UnitPrice] = 40
 where [t0].[CategoryName] = 'Produce'
 order by [t1].[UnitPrice] desc

 /* pr. 10 */
 select [t1].[CategoryName]
 from [dbo].[Categories] as [t1]

 select [t0].[ProductName], [t0].[UnitPrice], [t1].[CategoryName] -- as [CategoryName]
 from [dbo].[Products] as [t0]
 -- join [dbo].[Categories] AS [t1] on [t0].[CategoryID] = [t1].[CategoryID]
 left outer join [dbo].[Categories] AS [t1] on [t0].[CategoryID] = [t1].[CategoryID]
 -- right outer join [dbo].[Categories] AS [t1] on [t0].[CategoryID] = [t1].[CategoryID]
 -- where [t1].[CategoryName] = 'seafood' -- 'lol'
 where [t0].[UnitPrice] < 10
 order by [t0].[ProductName]
 -- order by [t1].[CategoryName], [t0].[UnitPrice]

 /* time out */ -- PRIKAZI SVE NARUDZBE KOJE SU NARUCILE TOFU PROIZVOD
  select *
  from [dbo].[Orders] as [t0]
  inner join [dbo].[Order Details] as [t1] on [t0].[OrderID] = [t1].[OrderID]
  inner join [dbo].[Products] as [t2] on [t1].[ProductID] = [t2].[ProductID]
  -- where [t1].[Quantity] > 10
  where [t2].[ProductName] = 'Tofu'
  ORDER BY	[t0].[OrderDate] asc

  -- Svi Narucioci koji su narucili tofu, poredani od zadnje narudzbe
  -- select *
  select  [t2].[ProductName], [tc].[CustomerID], [tc].[CompanyName], [tc].[ContactName], [t0].[OrderDate], [t1].[Quantity]
  from [dbo].[Customers] as [tc]
  inner join [dbo].[Orders] as [t0] on [tc].[CustomerId] = [t0].[CustomerId]
  inner join [dbo].[Order Details] as [t1] on [t0].[OrderID] = [t1].[OrderID]
  inner join [dbo].[Products] as [t2] on [t1].[ProductID] = [t2].[ProductID]
  -- where [t1].[Quantity] > 10
  where [t2].[ProductName] = 'Tofu'
  order by	[t0].[OrderDate] desc

  -- Koje je sve proizvode narucio jedan narucioc, npr. 'Paula Wilson', poredano po broju proizvoda
  select [t3].[ContactName], [t3].[CompanyName], [t2].[OrderDate], [t1].[Quantity], [t0].[ProductName]
  from [dbo].[Products] as [t0]
  inner join [dbo].[Order Details] as [t1] on [t0].[ProductID] = [t1].[ProductID]
  inner join [dbo].[Orders] as [t2] on [t1].[OrderID] = [t2].[OrderID]
  inner join [dbo].[Customers] as [t3] on [t2].[CustomerID] = [t3].[CustomerID]
  where [t3].[ContactName] = 'Paula Wilson' 
  order by [t1].[Quantity] desc

