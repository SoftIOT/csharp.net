﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMovie.Controllers
{
    public class HelloWorldController : Controller
    {
        // GET: HelloWorld
        public string Index()
        {

            return "This is my <b>default</b> action...";
        }

        public string Welcome()
        {
            return "This is the Welcome action method...";
        }

        public string WelcomePerson(string name, int numTimes = 1)
        {
            return HttpUtility.HtmlEncode("Hello " + name + ", NumTimes is: " + numTimes);
        }

        public ActionResult Indeks()
        {

            return View();
        }

        // [Route("api/indeksime/ime/{brojPuta}")] 
         // ovo je za web api, za mvc dodaj novu route u RouteConfig.cs
        // [Route("indeksime/ime/{brojPuta}")] 
        public ActionResult IndeksQ(string ime, int brojPuta = 1)
        {
            ViewBag.Poruka = "Hej " + ime;
            ViewBag.Brojac = brojPuta;
            return View();
        }
    }
}