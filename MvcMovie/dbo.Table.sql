﻿CREATE TABLE [dbo].[Movies]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Title] NVARCHAR(50) NULL, 
    [ReleaseDate] DATETIME NULL, 
    [Genre] NVARCHAR(50) NULL, 
    [Price] DECIMAL NULL
)
