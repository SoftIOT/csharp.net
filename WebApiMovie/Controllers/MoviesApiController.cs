﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApiMovie.Models;

namespace WebApiMovie.Controllers
{
    [EnableCors(origins: "https://webapimovie.gear.host", headers: "*", methods: "*")]
    public class MoviesApiController : ApiController
    {
        private appiotdbEntities db = new appiotdbEntities();

        // LIVE: http://webapimovie.gear.host/api/moviesapi
        //       http://webapimovie.gear.host/api/moviesapi/jsonlista

        // GET: api/MoviesApi
        public IQueryable<Movy> GetMovies()
        // public IHttpActionResult GetMovies()
        {
            return db.Movies;
            // return Ok(db.Movies);
            // return Json(db.Movies);
        }


        [HttpGet]
        [Route("api/moviesapi/jsonlista")]
        public IHttpActionResult JsonLista()
        {
            return Json(db.Movies);
        }

        // GET: api/MoviesApi/5
        [ResponseType(typeof(Movy))]
        public async Task<IHttpActionResult> GetMovy(int id)
        {
            Movy movy = await db.Movies.FindAsync(id);
            if (movy == null)
            {
                return NotFound();
            }

            return Ok(movy);
        }

        // PUT: api/MoviesApi/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMovy(int id, Movy movy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != movy.ID)
            {
                return BadRequest();
            }

            db.Entry(movy).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MoviesApi
        [ResponseType(typeof(Movy))]
        public async Task<IHttpActionResult> PostMovy(Movy movy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Movies.Add(movy);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = movy.ID }, movy);
        }

        // DELETE: api/MoviesApi/5
        [ResponseType(typeof(Movy))]
        public async Task<IHttpActionResult> DeleteMovy(int id)
        {
            Movy movy = await db.Movies.FindAsync(id);
            if (movy == null)
            {
                return NotFound();
            }

            db.Movies.Remove(movy);
            await db.SaveChangesAsync();

            return Ok(movy);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MovyExists(int id)
        {
            return db.Movies.Count(e => e.ID == id) > 0;
        }
    }
}